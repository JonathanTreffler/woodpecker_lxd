FROM alpine:3.19.0

RUN echo "https://dl-cdn.alpinelinux.org/alpine/latest-stable/community" >> /etc/apk/repositories

RUN apk add --update-cache bash util-linux dbus cgroup-tools qemu qemu-img qemu-system-x86_64 ovmf lxd qemu-ui-spice-core util-linux-misc qemu-chardev-spice qemu-hw-usb-redirect qemu-hw-display-virtio-vga virtiofsd lxd-scripts sgdisk

VOLUME /sys/fs/cgroup

RUN mv /usr/share/OVMF/OVMF_CODE.fd /usr/share/OVMF/OVMF_CODE.fd.original\
    && ln -s /usr/share/OVMF/OVMF.fd /usr/share/OVMF/OVMF_CODE.fd


#RUN rc-update add lxd default\
#  && rc-update add dbus \
#  && mkdir /run/openrc\
#  && touch /run/openrc/softlevel

# hijack entrypoint (/bin/sh) to redirect into lxd vm
RUN rm /bin/sh
COPY --chown=root:root --chmod=777 ./lxdfakeshell /bin/sh
